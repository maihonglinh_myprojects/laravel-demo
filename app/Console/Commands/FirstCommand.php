<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FirstCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:userphone';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update "null"-phone number to "No-Phone"';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = DB::table('test_db')->where
                ([
                    ['phone', '=', null]
                ])->update([
                    'phone'    => 'no-phone'
                ]);
    }
}