<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SecondCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:phonetonull';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update phone number from "no-phone" to "null"';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = DB::table('test_db')->where
                ([
                    ['phone', '=', 'no-phone']
                ])->update([
                    'phone'    => null
                ]);
    }
}
