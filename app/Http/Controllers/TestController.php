<?php

namespace App\Http\Controllers;

use App\Exports\testDBExport;
use App\Jobs\InsertDataJob;
use App\Models\TestModel;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;


class TestController extends Controller
{
    /** Part1 - Create API*/
    // First Test Function
    public function firstMsg(Request $request){
        try {            
            return [
                'status'    => "success",
                'data'      => "hello world"
            ];
        } catch (\Throwable $th) {
            return [
                'status'        => 'error',
                'errorMessage'  => $th
            ];
        }
    }
    // Second Test Function
    public function secondMsg(Request $request){
        try {            
            return [
                'status'    => "success",
                'data'      => "This is AllXOne"
            ];
        } catch (\Throwable $th) {
            return [
                'status'        => 'error',
                'errorMessage'  => $th
            ];
        }
    }
    
    
    /** -------------------------------------------------------------------------------------------------- */
    /** Part2 - Connect to DB*/
    public function insertData(Request $request){
        try {
            // Insert using query table
            // DB::table('test_db')->insert([
            //     'name'      => 'Linh',
            //     'email'     => 'linh4@allxone.com',
            //     'phone'     => null,
            //     'age'       => 25,
            //     'created_at'=> now()
            // ]);

            // Insert using Model
            $data = new TestModel();
            $data->name     = 'AllXOne';
            $data->email    = 'linh10@allxone.vn';
            $data->phone    = null;
            $data->age      = 25;
            $data->save();

            return [
                'status'    => "success",
                'data'      => "Save new Data"
            ];
        } catch (\Throwable $th) {
            return [
                'status'        => 'error',
                'errorMessage'  => $th->getMessage()
            ];
        }        
    }

    public function showData(Request $request){
        try {
            // getData using query table
            $data = DB::table('test_db')
                ->select('name', 'email', 'phone', 'age')
                // ->where('phone', '<>', null)
                ->get();

            // getData using Model
            // $data = TestModel::all()->where('phone', '<>', null);
            return response()->json($data);

        } catch (\Throwable $th) {
            return [
                'status'        => 'error',
                'errorMessage'  => $th->getMessage()
            ];
        }        
    }

    public function updateData(Request $request){
        try {

            // getData using query table
            // $data = DB::table('test_db')->where
            //     ([
            //         ['email', '=', 'linh@allxone.com']
            //     ])->update([
            //         'phone'     => '1234567890',
            //         'age'       => 20,
            //         'updated_at'=> now()
            //     ]);

            // getData using Model
            $data = TestModel::find('7');
            $data->age  = 10;
            $data->save();

            return response()->json($data);
        } catch (\Throwable $th) {
            return [
                'status'        => 'error',
                'errorMessage'  => $th->getMessage()
            ];
        }
    }
    
    public function deleteData(Request $request){
        try {

            // getData using query table
            // $data = DB::table('test_db')->where
            //     ([
            //         ['email', '=', 'linh@allxone.com']
            //     ])->delete();

            // getData using Model
            $data = TestModel::find('1');  // id of data in PostgreSql
            $data->delete();

            return [
                'status'        => 'success',
                'message'  => 'Delete successfully'
            ];
        } catch (\Throwable $th) {
            return [
                'status'        => 'error',
                'errorMessage'  => $th->getMessage()
            ];
        }
    }
    
    /** -------------------------------------------------------------------------------------------------- */
    /** Part3 - Get input Data from $request */
    public function addData(Request $request){
        try {
            $input = $request->all();
            $name  = Arr::get($input, 'name');
            $email = Arr::get($input, 'email');
            $phone = Arr::get($input, 'phone');
            $age   = Arr::get($input, 'age');

            DB::table('test_db')->insertOrIgnore([
                    'name'      => $name,
                    'email'     => $email,
                    'phone'     => $phone,
                    'age'       => $age,
                    'created_at'=> now()
            ]);
            return [
                'status'    => "success",
                'data'      => "Save new Data"
            ];
        } catch (\Throwable $th) {
            return [
                'status'        => "error",
                'errorMessage'  => $th->getMessage()
            ];
        }        
    }

    /** -------------------------------------------------------------------------------------------------- */
    /** Part4- Run Queue Jobs */
    public function importDataQueue(Request $request){
        // ..... Some other coding here .....
        // ----------------------------------

        // Code to add "new process" to queue
        $arr_data = [1,2,3,4,5];
        $job = new InsertDataJob($arr_data);
        dispatch($job);
    }

    /** -------------------------------------------------------------------------------------------------- */
    /** Part5- Import/Export CSV */
    public function importCsv(Request $request){
        try {
            $filename = resource_path('file/test_data.csv');
            $data_arr = $this->csvToArray($filename);

            foreach ($data_arr as $data) {
                DB::table('test_db')->insert([
                    'name'      => $data['name'],
                    'email'     => $data['email'],
                    'phone'     => $data['phone'],
                    'age'       => $data['age'],
                    'created_at'=> now()
                ]);
            }
            return [
                'status'    => "success",
                'message'   => "Import csv successfully"
            ];
        } catch (\Throwable $th) {
            return [
                'status'    => "error",
                'errorMsg'  => $th->getMessage()
            ];
        }
        
    }

    private function csvToArray($filename = '', $delimiter = ','){
        if (!file_exists($filename) || !is_readable($filename)){
            return false;
        }

        $header = null;
        $data = [];
        $file = fopen($filename, 'r');

        if($file != false){
            while(($line_data = fgetcsv($file, 'r', $delimiter)) != false){
                if(!$header){
                    $header = $line_data;
                }else{
                    array_push($data, array_combine($header, $line_data));
                }
            }
            fclose($file);
        }
        return $data;
    }

    public function exportCsv(Request $request){
        $data = DB::table('test_db')
                ->select('name', 'email', 'phone', 'age')
                ->orderBy('age', 'ASC')
                ->get();

        return Excel::download(new testDBExport($data), 'test_db.csv');
    }

}
