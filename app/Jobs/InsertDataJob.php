<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class InsertDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data_list;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($arr_data)
    {
        $this->data_list = $arr_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->importData();
    }

    private function importData()
    {
        try {
            $data_list = $this->data_list;
            foreach ($data_list as $data) {
                DB::table('test_db')->insert([
                    'name'      => 'Linh-AllXOne',
                    'email'     => 'maihonglinh'.$data.'@allxone.com',
                    'phone'     => null,
                    'age'       => 25,
                    'created_at'=> now()
                ]);
            }
            return [
                'status'    => "success",
                'message'   => "Save all data successfully"
            ];
        } catch (\Throwable $th) {
            return [
                'status'    => "error",
                'errorMsg'  => $th->getMessage()
            ];
        }
        
    }
}
