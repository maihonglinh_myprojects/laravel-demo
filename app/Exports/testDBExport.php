<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class testDBExport implements FromCollection, WithHeadings, WithMapping
{
    private $export_data;

    public function __construct($data)
    {
        $this->export_data = $data;
    }

    public function headings(): array{
        $headings = [
            'Name',
            'Email',
            'Phone',
            'Age'
        ];
        return $headings;
    }

    public function map($data): array{
        $data_arr = [
            $data->name,
            $data->email,
            $data->phone,
            $data->age
        ];
        return $data_arr;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return collect($this->export_data);
    }
}
