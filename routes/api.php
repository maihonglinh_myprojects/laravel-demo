<?php

use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api'], function($router){
    // part-1
    Route::get('firstMessage', [TestController::class, 'firstMsg']);
    Route::get('secondMessage', [TestController::class,'secondMsg']);
    // part-2
    Route::post('insertData', 'TestController@insertData');
    Route::get('showData', 'TestController@showData');
    Route::put('updateData', 'TestController@updateData');
    Route::delete('deleteData', 'TestController@deleteData');
    // part-3
    Route::post('addData', 'TestController@addData');
    // part-4
    Route::post('importData','TestController@importDataQueue');
    // part-5
    Route::post('importCsv', 'TestController@importCsv');
    Route::get('exportCsv', 'TestController@exportCsv');
});
